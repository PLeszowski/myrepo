package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

	private static int countPrimes;
	private static List<Integer> myList = new ArrayList<>();

	public static void main(String[] args) {
		printPrimes(100);
		System.out.println("number of prime numbers: " + countPrimes);

		for (Integer val: myList) {
			System.out.println("Prime numbers: " + val);
		}
		System.out.println("number of primes : " + myList.size());

	}

	private static void printPrimes(int n) {

		for (int i = 2; i < n; i++) {
			if (isPrime(i)) {
				System.out.println(i + " is prime");
			}
		}
	}

	private static boolean isPrime(int n) {
		if (n < 2) {
			return false;
		}
		for (int i = 2; i*i <= n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		countPrimes++;
		myList.add(n);
		return true;
	}

}
